from BasicVector import Vec2


def calculatePoints(pA, pB, pC, steps):
    points = []
    for i in range(steps):
        ac = Vec2.lerp(pA, pC, i/steps)
        bc = Vec2.lerp(pC, pB, i/steps)
        point = Vec2.lerp(ac, bc, i/steps)
        points.append(point)

    return points
