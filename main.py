import pygame
from BasicVector import Vec2
from pygame.locals import *
import time

from renderer import renderAll
from functions import calculatePoints


def simulate():
    WINDOW_SIZE = (600, 600)

    pygame.init()

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    pointA = Vec2(20, 300)
    pointB = Vec2(580, 300)
    pointC = Vec2(300, 20)

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("Bezier Curve")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 1 == 0:
            if mouse_pos is not None:
                pointC = Vec2(mouse_pos[0], mouse_pos[1])
            points = calculatePoints(pointA, pointB, pointC, 100)
            renderAll(window, points)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()
