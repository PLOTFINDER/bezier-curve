import pygame


def clear(window):
    window.fill((0, 0, 0))


def renderAll(window, points):
    clear(window)
    line_color = (255, 0, 0)
    last_point = None
    for point in points:
        if last_point is not None:
            pygame.draw.line(window, line_color, last_point.getPos(), point.getPos())
        last_point = point
